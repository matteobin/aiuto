BIN     = aiuto
VERSION = 1.0

PREFIX     = /usr/local
SED_PREFIX = \/usr\/local

all: install

clean:
	rm -f ${BIN}-${VERSION}.tar.gz

dist: clean
	mkdir ${BIN}-${VERSION}
	cp -R ${BIN} argomento-${BIN} LICENSE Makefile README ${BIN}-${VERSION}
	tar -cf ${BIN}-${VERSION}.tar ${BIN}-${VERSION}
	gzip ${BIN}-${VERSION}.tar
	rm -fr ${BIN}-${VERSION}

install:
	# bin
	mkdir -p ${DESTDIR}${PREFIX}/bin
	sed 's/^path="PREFIX/path="${SED_PREFIX}/' < ${BIN} > ${DESTDIR}${PREFIX}/bin/${BIN}
	chmod 755 ${DESTDIR}${PREFIX}/bin/${BIN}
	cp argomento-${BIN} ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/argomento-${BIN}

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/${BIN}
	rm -f ${DESTDIR}${PREFIX}/bin/argomento-${BIN}

.PHONY: all clean dist install uninstall
